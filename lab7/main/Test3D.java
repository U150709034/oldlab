package furkan.main;
import furkan.shapes3d.Cylinder;
import furkan.shapes.Circle;
import furkan.shapes3d.Box;

public class Test3D {

	public static void main(String[] args) {
		Circle circ = new Circle(5);
		System.out.println("Area of circle: " circ.area());
		
		Cylinder cylinder = new Cylinder(5,6);
		System.out.println("Area of cylinder: "cylinder.area());
		System.out.println("Volume of cylinder: "cylinder.volume());
		
		Circle circle = cylinder;
		System.out.println(circle.area());
		
		Object obj = cylinder;
		System.out.println(obj.toString());
		
		Box box = new Box (4,5,6);
		System.out.println(box.area());
		System.out.println(box.volume());
	}

}
