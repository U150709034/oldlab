package furkan.main;
import furkan.shapes.Circle;
import furkan.shapes.Rectangle;
import java.util.ArrayList;
import furkan.shapes.Drawing;
public class Main {
	
	public static void main(String args[]){
		Circle circle = new Circle(5);
		System.out.println(circle.area());
		
		Rectangle rectangle = new Rectangle(5,6);
		System.out.println(rectangle.area());
		
		ArrayList<Circle> circles = new ArrayList();
		circles.add(circle);
		circles.add(new Circle(6));
		circles.add(new Circle(7));
		System.out.println();
		
		
		Drawing drawing = new Drawing();
		for (int i= 0 ; i < circles.size(); i++ ){
			Circle circ= circles.get(i);
			drawing.addCircle(circ);
			
		}
		drawing.printAreas();
	}
	
}
