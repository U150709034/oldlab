package furkan.shapes;
import java.util.ArrayList;
import furkan.shapes.Circle;
import furkan.shapes.Rectangle;

public class Drawing {
	ArrayList <Circle> circles = new ArrayList();
	ArrayList <Rectangle> rectangles = new ArrayList(); 
	public Drawing() {
		// TODO Auto-generated constructor stub
	}
	
	public void addCircle(Circle circ){
		circles.add(circ);
	}
	
	public void addRectangle(Rectangle rect){
		rectangles.add(rect);
		
	}
	public void printAreas() {
		for (Circle circles : circles){
			System.out.println(circles.area());
		}
	}
	public void printRadiuses() {
		for (Circle circle : circles){
			System.out.println(circle.radius);
		}
	}	
}